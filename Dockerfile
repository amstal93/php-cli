FROM registry.gitlab.com/foobox/php-common:master

ARG PHP_VERSION=7.3.11

ENV PHP_INPUT_TIME_LIMIT=60 \
    PHP_MEMORY_LIMIT=256M \
    PHP_TIME_LIMIT=60

COPY src /
RUN php-cli-setup
ENTRYPOINT ["php"]
